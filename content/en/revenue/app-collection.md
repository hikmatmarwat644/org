---
title: App Collection
description: Grey Software offers a collection of actively supported freemium apps and utilities
category: Revenue Channels
position: 2003
---

## Overview

### Hypothesis

People who do creative, academic, or professional work on the web want to use apps and utilities that are actively
supported by a trusted brand rather than independently developed apps and utilities with no guarantee of bug fixes and
new features.

If we create a collection of software apps and utilities with a generous amount of free features and offer premium
functionality for a monthly subscription, we can generate sustainable revenue for Grey Software.

### What is your value proposition?

If you do creative, academic, or professional work on the web, you probably use some independently developed apps or
utilities which don't come from a trusted brand that offers support or actively develops feature requests.

Instead, Grey Software offers a growing collection of freemium software apps and utilities that are actively supported
by a trusted brand.

### Strategy

- We build useful open source products for the public
- We generate revenue by offering premium features on our products for monthly subscribers.
- We aim to update our catalog of apps by signing existing open-source apps and incubating original software ideas from
  our team members and students
- If we offer a valuable collection of products for a monthly subscription, we can achieve economies of scale in a
  similar manner to Google GSuite, Microsoft Office365, or Adobe Creative Cloud, but for public consumer apps and
  utilities aimed at young students and entrepreneurs.

## Customers and Partners

### Who are your personas?

#### Yasmine The University Student

Yasmine is a university computer science student who is actively engaged in academic and creative work online.

She uses independently developed apps and utilities like News Feed Eradicator, Tab Brightness Controller, and others.

Yasmine is comfortable with paying for software if it benefits her life, and she is likely to support a trusted
organization building a suite of useful apps and utilities for people like her.

Yasmine is amazed when she learns about Grey Software, which not only builds useful apps and utilities for people like
her, but also publishes valuable and free educational content for students like her worldwide. She happily becomes a
supporter on the $2 or $5/month tiers.

#### Jerry The Enthusiast

Jerry has been working with computers for most of his life, and he has crafted a good living situation for himself from
his time in the technology industry.

Jerry has seen the evolution of computer and software technology. That is why he respects and often financially supports
institutions like the Apache Software Foundation, the Free Software Foundation, Mozilla, and other impactful tech
organizations that have benefited society.

Jerry is excited when he learns about Grey Software, an organization working with students worldwide to create software
that he can trust, love, and learn from. He's excited about trying out the software that students are building and
decides to support on the $10 or $20/month tiers.

### Impact & Traction

#### Focused Browsing

https://grey.software/focused-browsing/

We created and published an improved version of News Feed Eradicator, a competing productivity web extension that
achieved over 200,000 downloads with no significant marketing

#### Grey Docs

https://grey-docs.grey.software

We created a framework that allowed us to build beautiful markdown-based websites that now power our education
ecosystem.

#### The Grey Software Nest

https://gitlab.com/grey-software/nest

We have worked on a number of project concepts with students that can be built upon and published.
