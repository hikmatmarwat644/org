---
title: 'Week of 10/18/2021'
description: Catch up with the latest updates from Grey Software!
category: 'Weekly Updates'
position: 3000
---

## Overview

This week, we published another revised edition of our [`/pitch`](https://grey.software/pitch), and we published our
bank account transaction history to the public for the first time!

<cta-button text="v4.2 Milestone" link="https://gitlab.com/groups/grey-software/-/milestones/2"></cta-button>
<cta-button text="Pitch" link="https://grey.software/pitch"></cta-button>

> **Reminder of our major goals from last week**
>
> 1. Refine /pitch and start campaigning for Founding Partners
> 2. Continue outlining workshops and filling gaps in our docs
> 3. Publish Yearly Financials

### What we did this past week

1. Published a new iteration of [`/pitch`](https://grey.software/pitch)

![Pitch Slide](https://grey.software/pitch/2.png)

2. We published our [financials page](/financials) with a history of our bank account transactions

3. We created a flowchart component for our docs that fits more with our branding

4. Focused Browsing now has its own [Twitter Account](https://twitter.com/focusedbrowsing)

### Our goals for Next Week

1. Publish our /pitch and /founding story as Twitter threads

2. Refine our financials page and file our yearly revenue with the Canadian Government!

3. Continue outlining workshop content and filling gaps in our documentation!

## Team Updates

### Faraz

This week I worked on the [automation dashboard's](https://gitlab.com/groups/grey-software/-/epics/5) issue
[#6](https://gitlab.com/grey-software/automation/-/issues/6) to list all the active organization's issues to the
dashboard with the options to filter issue based on milestones and labels. Each list shows the complete overview of
issue. ![](https://i.imgur.com/gqvzPqM.png)

### Laiba

This week I collaborated further on revising and refining the
[resources' contribution](https://gitlab.com/grey-software/resources/-/issues/13) and
[gitlab management](https://gitlab.com/grey-software/resources/-/issues/14) documents and made some more polish updates.

The final result of flowcharts that fit more with Grey Software branding

#### How we process issues at Grey software

<flow-chart name="issue-lifecycle"></flow-chart>

#### The following flow chart briefly describes how you can contribute!

<flow-chart name="contributing-resources"></flow-chart>
